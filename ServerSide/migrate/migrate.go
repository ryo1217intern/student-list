package main

import (
	"ServerSide/db"
	"ServerSide/model"
	"fmt"
)


func main() {
	dbConn := db.NewDB()

	defer fmt.Println("Success migrate database")
	defer db.CloseDB(dbConn)

	dbConn.AutoMigrate( &model.User{})
}