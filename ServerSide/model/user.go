package model

import "time"

// User defines the structure for user data in the system.
type User struct {
    ID   uint   `json:"id" gorm:"primaryKey"` // UserID is the unique identifier for the user and is the primary key in the database.
    Email    string `json:"email" gorm:"unique"`  // Email is a unique identifier for the user and is used for login.
    Password string `json:"password"`             // Password is the user's password for authentication, stored securely.

    Role      string `json:"role"`           // Role represents the user's role within the system (e.g., admin, student, teacher).
    Classroom string `json:"classroom_id"`   // Classroom links the user to a specific classroom entity.

    CreatedAt time.Time `json:"created_at"` // CreatedAt is the timestamp of when the user was created in the system.
    UpdatedAt time.Time `json:"updated_at"` // UpdatedAt is the timestamp of the last update to the user's data.
}

// UserResponse defines the structure for how user data will be sent to the client.
type UserResponse struct {
		ID        uint   `json:"id" gorm:"primaryKey"`              // ID of the user, included in the response for identification.
    Email     string `json:"email"`           // Email of the user, included in the response for identification.
    Role      string `json:"role"`            // Role of the user, included in the response to define user permissions.
    Classroom string `json:"classroom_id"`    // Classroom ID to identify which classroom the user belongs to.
}
