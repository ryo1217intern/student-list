package main

import (
	"ServerSide/controller"
	"ServerSide/db"
	"ServerSide/repository"
	"ServerSide/router"
	"ServerSide/usecase"
)

func main() {
	db := db.NewDB()
	userReposiotry := repository.NewUserRepository(db)
	userUsecase := usecase.NewUserUsecase(userReposiotry)
	userController := controller.NewUserController(userUsecase)
	e := router.NewRouter(userController)
	e.Logger.Fatal(e.Start(":8080"))
}